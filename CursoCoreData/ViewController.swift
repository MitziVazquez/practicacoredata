//
//  ViewController.swift
//  CursoCoreData
//
//  Created by MAC on 1/6/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
       // let dato = Bundle.main.object(forInfoDictionaryKey: "Nombre") as? String
        //print("\(String(describing: dato!))")
        // Do any additional setup after loading the view.
        
        let datos = Bundle.main.path(forResource: "Datos", ofType: "plist")
        let recurso = NSArray(contentsOfFile: datos!)!
        
        for d in recurso {
            let dic = d as![String:AnyObject]
            
            print("el nombre es :\(dic["nombre"]!) y edad : \(dic["edad"]!)")
        }
    }


}

