//
//  TableViewController.swift
//  CursoCoreData
//
//  Created by MAC on 1/6/20.
//  Copyright © 2020 MAC. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UITableViewController {

    var manageObjects:[NSManagedObject] = []
    var lista:[String] = ["Merida","Puebla","Iztapalapa❤"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        
        let fetchrequest = NSFetchRequest<NSManagedObject>(entityName: "Lista") //trbajar sobre esta entidad, condicion que se quiere buscar
        do{
           
            manageObjects = try managedContext.fetch(fetchrequest)
            
        }catch let error as NSError{
            print("\(error.userInfo)")
        }
        
        //llamado a appDelegate
        
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return manageObjects.count //recuperar lo que hay y se agrega
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        //cell.textLabel?.text = lista[indexPath.row]
        let objcore = manageObjects[indexPath.row]
        var nombre = objcore.value(forKey: "palabra")
        var edad = objcore.value(forKey: "edad")
    
        cell.textLabel?.text = "\(nombre!)\(edad!)"
       
        return cell
    }
//btn de agregar
    
    @IBAction func add(_ sender: Any) {
        
        let alerta = UIAlertController(title: "Nueva palabra", message: "agregar palabra nueva", preferredStyle: .alert)
        let guardar = UIAlertAction(title: "Agregar", style: .default, handler: {
            (action: UIAlertAction) -> Void in
            
            let textField = alerta.textFields![0]
            let textField1 = alerta.textFields![1]
            //self.lista.append(textField!.text!)
            self.guardarPalabra(palabra: textField.text!, edad: Int16(textField1.text!)!)
            self.tableView.reloadData()
        })
        let cancelar = UIAlertAction(title: "Cancelar", style: .default)
        {(action:UIAlertAction) -> Void in }
        
        alerta.addTextField{(UITextField:UITextField) -> Void in}
        alerta.addTextField{(UITextField:UITextField) -> Void in}
            //UITextField.keyboardType = .numberPad
        
        
        alerta.addAction(guardar)
        alerta.addAction(cancelar)
        
        present(alerta, animated: true, completion: nil)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //self.lista.remove(at: indexPath.row)
            
            // Delete the row from the data source
            let objcore = manageObjects[indexPath.row]
            
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            let managedContext = appDelegate!.persistentContainer.viewContext
            
            managedContext.delete(objcore) // elimina del coredata
            manageObjects.remove(at: indexPath.row) // lo elimina del arreglo
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func guardarPalabra(palabra: String, edad: Int16) {
        if edad < 12 || edad > 100 {
            print("no tiene la edad dentro del rango")
        }else {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Lista", in: managedContext)!
        let managedObject = NSManagedObject(entity: entity, insertInto: managedContext)
    
        
        managedObject.setValue(palabra, forKeyPath: "palabra")
        managedObject.setValue(edad, forKeyPath: "edad")
        
        
        do{
           
            try managedContext.save()
            manageObjects.append(managedObject)
            
        }catch let error as NSError {
            print("\(error.userInfo)")
        }
    }
    
    
   }

}
